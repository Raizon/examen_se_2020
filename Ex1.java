package dwadaw;


public class Ex1 implements Runnable{
	private String name;
	
	public Ex1(String name) {
		this.name = name;
	}
	
	public void run() {
		int i = 1;
		while(i <= 12) {
			System.out.println(this.name +
					" - " + i);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			i++;
		}
	}
}

class ADriver {
	public static void main(String[] args) {
		Ex1 aVar1 = new Ex1("AThread1");
		Thread t = new Thread(aVar1);
		t.start();
		Ex1 aVar2 = new Ex1("AThread2");
		t = new Thread(aVar2);
		t.start();
	}

}
